import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';
import { Navigation } from 'react-native-navigation';

import { registerScreens } from './src/screens';

registerScreens();

Navigation.startSingleScreenApp({
  screen:{
    screen: 'TopTabsScreen',
    title: 'Wallpapers',
    topTabs: [
      {
        screenId: 'Recent',
        title: 'Recent',
      },

      {
        screenId: 'Featured',
        title: 'Featured'
      },
      {
        screenId: 'Categories',
        title: 'Categories'
      }
    ],
  },
  appStyle: {
    topTabTextColor: '#fff',
    selectedTopTabTextColor: '#228b22',
  },
  portraitOnlyMode: true
});