package com.amazingwallz;

import android.content.Context;
import android.media.AudioManager;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.app.WallpaperManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import java.net.URL;
import android.net.Uri;
import java.io.File;
import java.io.IOException;

import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

public class SetWallpaperModule extends ReactContextBaseJavaModule {
    private static final String TAG = "SetWall";

    public SetWallpaperModule(ReactApplicationContext reactContext) {
        super(reactContext);
        
    }

    @Override
    public String getName() {
        return TAG;
    }

    @ReactMethod
    public void setImageAsWallpaper(String uri){
        Uri contentUri = Uri.fromFile(new File(uri));
        Intent intent = new Intent(Intent.ACTION_ATTACH_DATA);
        intent.setDataAndType(contentUri, "image/*");
        intent.putExtra("mimeType", "image/*");
        getCurrentActivity().startActivity(Intent.createChooser(intent, "Set As"));
    }
}