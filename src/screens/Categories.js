import React, {Component} from 'react';
import {View, Text, Image, FlatList, TouchableOpacity, Dimensions, AppRegistry} from 'react-native';
import Unsplash, {toJson} from 'unsplash-js/native';
import { List, ListItem, Card } from 'react-native-elements';
import {AdMobBanner} from 'react-native-admob';
import RNFetchBlob from 'react-native-fetch-blob';

const WIDTH = Dimensions.get('window').width;

export default class Categories extends Component{

    constructor(props){
        super(props);
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
    }

    onNavigatorEvent(event){
        if(event.type == 'NavBarButtonPress'){
            if(event.id == 'search'){
                this.props.navigator.push({
                    screen: 'Search',
                    title: 'Search'
                });
            }
        }
        if(event.id == 'tabSelected'){
            console.log('Categories Tab Selected')
            this.props.navigator.setButtons({
                rightButtons: [
                    {
                        icon: require('../drawables/search.png'),
                        id: 'search',
                    },
                ]
            })
        }
    }

    componentWillUnmount(){
        RNFetchBlob.fs.unlink(RNFetchBlob.fs.dirs.CacheDir).then(()=>{
            console.log('Cache file flushed')
        })
    }
 
    render(){
        const list = [
        {
            key: 'Nature',
            title: 'Nature',
            image_url: 'https://source.unsplash.com/featured/?nature'
        },
        {
            key: 'Animals',
            title: 'Animals',
            image_url: 'https://source.unsplash.com/featured/?animals'
        },
        {
            key: 'Landscape',
            title: 'Landscape',
            image_url: 'https://source.unsplash.com/featured/?landscape'
        },
        {
            key: 'Love',
            title: 'Love',
            image_url: 'https://source.unsplash.com/featured/?love'
        },
        {
            key: 'Rain',
            title: 'Rain',
            image_url: 'https://source.unsplash.com/featured/?rain'
        },
        {
            key: 'Food',
            title: 'Food',
            image_url: 'https://source.unsplash.com/featured/?food'
        },
        {
            key: 'Space',
            title: 'Space',
            image_url: 'https://source.unsplash.com/featured/?space'
        },
        {
            key: 'Sky',
            title: 'Sky',
            image_url: 'https://source.unsplash.com/featured/?sky'
        },
        ]
        const { cardContent, textStyle, imageStyle, adStyle } = styles;

        return(
            <View>
            <List containerStyle={{marginTop:0, marginBottom: 0}}>
                <FlatList
                    data={list}
                    renderItem={({ item }) => (
                        <TouchableOpacity
                        onPress = {() => 
                            this.props.navigator.push({
                                title: item.title,
                                screen: 'ListWallpapers',
                                passProps:{
                                    searchKey: item.title
                                },
                                animated: true,
                                animationType: 'slide-horizontal'
                            })
                        }>
                            <View style={cardContent}>
                                <Image
                                    source={{uri: item.image_url}}
                                    style={imageStyle}>
                                    <View>
                                        <Text style={textStyle}>{item.title}</Text>
                                    </View>
                                </Image>
                            </View>
                        </TouchableOpacity>
                    )}
                    keyExtractor={item => item.key}
                    numColumns={1}
                />
            </List>
            <View style={adStyle}>
                <AdMobBanner
                    bannerSize="smartBanner"
                    adUnitID="ca-app-pub-4831095411086994/1897006775"
                    didFailToReceiveAdWithError={()=> console.log('Banner Error')} />
            </View>
            </View>
        );
    }
}

const styles = {
    cardContent: {
        flex:1,
        backgroundColor: '#228b22'
    },
    textStyle: {
        fontSize: 30, 
        fontWeight: 'bold',
        fontFamily: 'monospace',
        color: '#fff', 
        textAlign: 'center',
        textShadowColor: 'black',
        textShadowOffset: {width: 2, height: 2},
        textShadowRadius: 5
    },
    imageStyle: {
        height: WIDTH/2, 
        width: WIDTH,
        alignItems: 'center',
        justifyContent:'center'
    },
    adStyle: {
        position: 'absolute',
        bottom: 0,
    }
}
