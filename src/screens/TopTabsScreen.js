import React, {Component} from 'react';
import Unsplash from 'unsplash-js/native';
import { 
    AdMobBanner, 
    AdMobInterstitial, 
    PublisherBanner,
    AdMobRewarded
  } from 'react-native-admob';
  import {Unsplash_AppID, Unsplash_Secret, Unsplash_CallbackUrl} from '../Utils';

unsplash = new Unsplash({
    applicationId: Unsplash_AppID,
    secret: Unsplash_Secret,
    callbackUrl: Unsplash_CallbackUrl
});

authenticationUrl = unsplash.auth.getAuthenticationUrl([
    "public",
]);

export default class TopTabsScreen extends Component{
    static navigatorButtons = {
        rightButtons:[
            {
                icon: require('../drawables/search.png'),
                id: 'search',
            },
        ]
    };

    static navigatorStyle = {
        statusBarColor: '#212121',
        statusBarTextColorScheme: 'light',
        navBarBackgroundColor: '#212121',
        navBarTextColor: '#fff',
        navBarButtonColor: '#fff'
    }

    constructor(props){
        super(props);
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
    }

    onNavigatorEvent(event){
        if(event.type == 'NavBarButtonPress'){
            if(event.id == 'search'){
                this.props.navigator.push({
                    screen: 'Search',
                    title: 'Search'
                });
            }
        }
    }

    componentWillUnmount(){
        RNFetchBlob.fs.unlink(RNFetchBlob.fs.dirs.DownloadDir + '/' + `${this.props.itemID}` + '.jpg').then(()=>{
            console.log('Cache file flushed')
        })
        RNFetchBlob.fs.unlink(RNFetchBlob.fs.dirs.CacheDir).then(()=>{
            console.log('Cache file flushed')
        })
    }

    render(){
        return(
            <View>
            </View>
        );
    }
}