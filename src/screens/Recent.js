import React, {Component} from 'react';
import {
    View, 
    Text, 
    Dimensions, 
    TouchableOpacity, 
    Linking, 
    CameraRoll, 
    Alert,
    FlatList
    } from 'react-native';
import Image from 'react-native-image-progress';
import * as Progress from 'react-native-progress';
import {List, ListItem, Button} from 'react-native-elements';
import Unsplash, {toJson} from 'unsplash-js/native';
import { AdMobBanner } from 'react-native-admob';
import {AdMob_AppID, AdMob_UnitID} from '../Utils';
import RNFetchBlob from 'react-native-fetch-blob';

const WIDTH = Dimensions.get('window').width;

export default class Recent extends Component{

    static navigatorStyle = {
        statusBarColor: '#212121',
        statusBarTextColorScheme: 'light',
        navBarBackgroundColor: '#212121',
        navBarTextColor: '#fff',
        navBarButtonColor: '#fff'
    }

    constructor(props){
        super(props);

        this.state = {
            walls: [],
            page: 1,
            perPage: 30,
            loading: true,
            filter: 'latest'
        };
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
    }

    onNavigatorEvent(event){
        if(event.type == 'NavBarButtonPress'){
            if(event.id == 'search'){
                this.props.navigator.push({
                    screen: 'Search',
                    title: 'Search'
                });
            }
            if(event.id == 'latest'){
                this.setState({
                    filter: 'latest',
                    walls: [],
                    page: 1
                }, ()=> {
                    this.fetchWalls();
                })
            }
            if(event.id == 'oldest'){
                this.setState({
                    filter: 'oldest',
                    walls: [],
                    page: 1
                }, ()=> {
                    this.fetchWalls();
                })
            }
            if(event.id == 'popular'){
                this.setState({
                    filter: 'popular',
                    walls: [],
                    page: 1
                }, ()=> {
                    this.fetchWalls();
                })
            }
        }
        if(event.id == 'tabSelected'){
            console.log('Recent Tab Selected')
            this.props.navigator.setButtons({
                rightButtons:[
                    {
                        icon: require('../drawables/search.png'),
                        id: 'search',
                    },
                    {
                        id: 'latest',
                        title: 'Latest',
                        showAsAction: 'never'
                    },
                    {
                        id: 'oldest',
                        title: 'Oldest',
                        showAsAction: 'never'
                    },
                    {
                        id: 'popular',
                        title: 'Popular',
                        showAsAction: 'never'
                    }
                ]
            })
        }
    }

    fetchWalls(){
       unsplash.photos.listPhotos(this.state.page, this.state.perPage, this.state.filter)
        .then(toJson)
        .then(json => {
            var wallImages = [];
            for(var x in json){
                wallImages.push(json[x])
            }
            this.setState({
                walls: [...this.state.walls, ...wallImages],
                loading: false
            });
            console.log(wallImages);
        })
        .catch(err => {
            console.log(err);
        });
    }

    handleLoadMore = () => {
        this.setState({
            page: this.state.page + 1,
        }, () =>{
            this.fetchWalls();
        })
    };

    renderIndicator = () => {
        return(
            <View style={{flex:1, justifyContent: 'center', alignItems: 'center'}}>
                <Progress.CircleSnail 
                    color='#228b22' 
                    size={80}
                />
            </View>
        );
    };

    renderFooter = () => {
        return(
            <View style={{flex:1, justifyContent: 'center', alignItems: 'center'}}>
                <Progress.CircleSnail 
                    color='#228b22' 
                    size={40}
                />
            </View>
        );
    };

    componentWillMount(){
        this.fetchWalls();
    }

    componentDidMount(){
        this.props.navigator.setButtons({
            rightButtons:[
                {
                    icon: require('../drawables/search.png'),
                    id: 'search',
                },
                {
                    id: 'latest',
                    title: 'Latest',
                    showAsAction: 'never'
                },
                {
                    id: 'oldest',
                    title: 'Oldest',
                    showAsAction: 'never'
                },
                {
                    id: 'popular',
                    title: 'Popular',
                    showAsAction: 'never'
                }
            ]
        })
    }
    
    componentWillUnmount(){
        RNFetchBlob.fs.unlink(RNFetchBlob.fs.dirs.CacheDir).then(()=>{
            console.log('Cache file flushed')
        })
    }

    render(){
        const {container, imageStyles, adStyle} = styles;
        if(this.state.walls.length < 1){
            return this.renderIndicator();
        }
        else{
        return(
        <View>
        <List containerStyle={{marginTop: 0}}>
            <FlatList
                data={this.state.walls}
                renderItem={({ item }) => (
                    <TouchableOpacity
                    onPress={() => this.props.navigator.push({
                        screen: 'DisplayImage',
                        passProps:{
                            itemID: item.id,
                            linkRaw: item.urls.raw,
                            linkRegular: item.urls.regular,
                            userName: item.user.name,
                            userLink: item.user.links.html,
                            userProfileImage: item.user.profile_image.small,
                            userLocation: item.user.location,
                            itemLikes: item.likes,
                            date: item.created_at,
                        },
                        animated: true,
                        animationType: 'slide-horizontal'
                    }
                    )}>
                        <View style={container}>
                            <Image
                                source={{uri: item.urls.small}}
                                style={imageStyles}
                            />
                        </View>
                    </TouchableOpacity>
                )}
                numColumns={2}
                keyExtractor={(item, index) => index}
                onEndReached={this.handleLoadMore}
                onEndThreshold={0.2}
                ListFooterComponent={this.renderFooter}
                />
        </List>
        <View style={adStyle}>
            <AdMobBanner
                bannerSize="smartBanner"
                adUnitID="ca-app-pub-4831095411086994/1897006775"
                didFailToReceiveAdWithError={()=> console.log('Banner Error')} />
        </View>
        </View>
        );
        }
    }
}

const styles={
    imageStyles:{
        height: WIDTH / 2,
        width: WIDTH / 2,
    },
    container: {
        flex:1,
        margin: 0,
    },
    adStyle: {
        position: 'absolute',
        bottom: 0,
        width: WIDTH
    }
}
