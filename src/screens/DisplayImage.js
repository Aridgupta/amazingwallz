import React, {Component} from 'react';
import {
    View, 
    Text, 
    Dimensions, 
    TouchableOpacity,
    CameraRoll, 
    ToastAndroid,
    PermissionsAndroid,
    Linking,
    ScrollView
    } from 'react-native';
import Image from 'react-native-image-progress';
import * as Progress from 'react-native-progress';
import {List, ListItem, Card, Button, Avatar} from 'react-native-elements';
import Icon from 'react-native-vector-icons';
import Unsplash, {toJson} from 'unsplash-js/native';
import {setImageAsWallpaper} from '../Wallpaper.js';
import RNFetchBlob from 'react-native-fetch-blob';
import { AdMobBanner } from 'react-native-admob';
import {AdMob_AppID, AdMob_UnitID} from '../Utils';

const HEIGHT = Dimensions.get('window').height;
const WIDTH = Dimensions.get('window').width;

export default class DisplayImage extends Component{
    static navigatorStyle = {
        statusBarColor: '#212121',
        statusBarTextColorScheme: 'light',
        navBarBackgroundColor: '#212121',
        navBarTextColor: '#fff',
        navBarButtonColor: '#fff'
    };
    
    static navigatorButtons={
        rightButtons:[
            {
                icon: require('../drawables/download.png'),
                id: 'download',
                showAsAction: 'ifRoom'
            },
            {
                icon: require('../drawables/wallpaper.png'),
                id: 'setAs',
                //title: 'Set As',
                showAsAction: 'ifRoom'
            }
        ]
    }

    constructor(props){
        super(props);
        this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
        this.state = {
            received: 0,
            total: 1,
            showProgress: false,
            buttonEnable: true
        }
    }

    onNavigatorEvent(event){
        if(event.type == 'NavBarButtonPress'){
            if(event.id == 'download'){
                if(this.state.buttonEnable){
                    this.handleDownload();
                }
            }
            if(event.id == 'setAs'){
                if(this.state.buttonEnable){
                    this.handleSetImageAsWallpaper();
                }
            }
        }
    }

    async handleSetImageAsWallpaper(){
        const granted = await PermissionsAndroid.check(
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE)
        if(granted){
            this.setImage();
        }
        else{
            const permission = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                {
                    'title': 'AmazingWallz Storage Permission',
                    'message': 'Allow AmazingWallz to access photos,' + 
                                'media, and files on your device?'
                }
            )
            if(permission === PermissionsAndroid.RESULTS.GRANTED){
                this.setImage();
            }
            else{
                console.log("Storage Permission Denied")
            }
        }
    }

    async handleDownload(){
        const granted = await PermissionsAndroid.check(
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE)
        if(granted){
            this.saveImage();
        }
        else{
            const permission = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
                {
                    'title': 'AmazingWallz Storage Permission',
                    'message': 'Allow AmazingWallz to access photos,' + 
                                'media, and files on your device?'
                }
            )
            if(permission === PermissionsAndroid.RESULTS.GRANTED){
                this.saveImage();
            }
            else{
                console.log("Storage Permission Denied")
            }
        }
    }

    setImage = () => {
        RNFetchBlob
        .config({
            fileCache: true,
            appendExt: 'jpg',
            path: RNFetchBlob.fs.dirs.DownloadDir + '/' + `${this.props.itemID}` + '.jpg'
        })
        .fetch('GET', this.props.linkRaw)
        .progress((received, total) => {
            this.setState({
                received: received,
                total: total,
                showProgress: true,
                buttonEnable: false
            });
        })
        .then((resp) => {
            console.log(resp.path())
            const uri = resp.path()
            this.setState({
                showProgress: false,
                buttonEnable: true
            })
            setImageAsWallpaper(uri)
        })
    };

    saveImage = () => {
        RNFetchBlob
        .config({
            fileCache: true,
            appendExt: 'jpg',
            path: RNFetchBlob.fs.dirs.DownloadDir + '/' + `${this.props.itemID}` + '.jpg'
        })
        .fetch('GET', this.props.linkRaw)
        .progress((received, total) => {
            this.setState({
                received: received,
                total: total,
                showProgress: true,
                buttonEnable: false
            });
        })
        .then((resp) => {
            console.log(RNFetchBlob.fs.dirs)
            /* CameraRoll.saveToCameraRoll('file://'+resp.path())
            .then(ToastAndroid.show('Image saved to gallery', ToastAndroid.SHORT))
            .catch(err => console.log('err:', err)) */
            RNFetchBlob.fs.mkdir(RNFetchBlob.fs.dirs.PictureDir + '/AmazingWallz')
            .then(()=>{
                console.log('Directory created')
            })
            .catch((err)=>{
                console.log('Directory Exists')
            })
            
            RNFetchBlob.fs.mv(resp.path(), RNFetchBlob.fs.dirs.PictureDir + '/AmazingWallz' + '/' + `${this.props.itemID}` + '.jpg')
            .then(()=>{
                ToastAndroid.show('Image saved to gallery', ToastAndroid.SHORT)
            })
            this.setState({
                showProgress: false,
                buttonEnable: true
            })
            RNFetchBlob.fs.scanFile([{ path: RNFetchBlob.fs.dirs.PictureDir + '/AmazingWallz' + '/' + `${this.props.itemID}` + '.jpg' }])
            /* RNFetchBlob.fs.unlink(resp.path()).then(()=>{
                console.log('Cache file flushed')
            }) */
        })
    };

    handleOpenUnsplash = () => {
        Linking.openURL(this.props.userLink+'?utm_source=amazingwallz&utm_medium=referral&utm_campaign=api-credit')
        .catch((err) => {
            console.log('An error occurred', err)
        })
    };

    componentWillUnmount(){
        RNFetchBlob.fs.unlink(RNFetchBlob.fs.dirs.DownloadDir + '/' + `${this.props.itemID}` + '.jpg').then(()=>{
            console.log('Cache file flushed')
        })
        RNFetchBlob.fs.unlink(RNFetchBlob.fs.dirs.CacheDir).then(()=>{
            console.log('Cache file flushed')
        })
    }

    render(){
        const {container, imageStyle, description, iconStyle, listItemStyle} = styles;
        if(this.state.showProgress){
            progress =  <View style={{top: 0, bottom: 0}}>
                        <Progress.Bar progress={this.state.received/this.state.total} width={WIDTH} borderRadius={1} borderColor='#fff' color='#228b22' />
                        </View>;
        }
        else{
            progress = <View></View>
        }
        return(
            <View style={container}>
                {progress}
                <ScrollView>
                <View  style={{flex:1}}>
                    <Image 
                        source={{uri:this.props.linkRegular}}  
                        style={imageStyle} 
                        resizeMode='cover'
                        indicator={Progress.CircleSnail}
                    />
                </View>
                <View style={description}>
                    <List containerStyle={{marginTop:0}}>
                        <ListItem hideChevron roundAvatar avatar={{uri:this.props.userProfileImage}} title={this.props.userName + ' / Unsplash'} containerStyle={listItemStyle} titleStyle={{fontWeight: 'bold', fontSize: 16}} onPress={this.handleOpenUnsplash}/>
                        <ListItem hideChevron leftIcon={{name: 'date-range', color: 'black'}} title={this.props.date.slice(0,10)} containerStyle={listItemStyle} titleStyle={{fontSize: 16}}/>
                        <ListItem hideChevron leftIcon={{name: 'location-on', color: 'black'}} title={this.props.userLocation} containerStyle={listItemStyle} titleStyle={{fontSize: 16}}/>
                        <ListItem hideChevron leftIcon={{name: 'favorite', color: 'black'}} title={this.props.itemLikes} containerStyle={listItemStyle} titleStyle={{fontSize: 16}}/>
                    </List>
                </View>
                </ScrollView>
                <AdMobBanner
                    bannerSize="smartBanner"
                    adUnitID="ca-app-pub-4831095411086994/1897006775"
                    didFailToReceiveAdWithError={()=> console.log('Banner Error')} />
            </View>
        );
    }
}

const styles = {
    container:{
        flex: 1,
    },
    imageStyle:{
        flex: 1,
        width: WIDTH,
        height: HEIGHT/1.5
    },
    description:{
        flex: 1,
        width: WIDTH,
        backgroundColor: '#fff',
        elevation: 10
    },
    listItemStyle:{
        elevation: 0,
        zIndex: 0,
        margin:0,
        borderTopWidth: 0,
        borderBottomWidth: 0
    }
}