import { Navigation } from 'react-native-navigation';

import TopTabsScreen from './TopTabsScreen';
import Recent from './Recent';
import Categories from './Categories';
import Featured from './Featured';
import ListWallpapers from './ListWallpapers';
import DisplayImage from './DisplayImage';
import Search from './Search';

export function registerScreens(){
    Navigation.registerComponent('TopTabsScreen', () => TopTabsScreen);
    Navigation.registerComponent('Recent', () => Recent);
    Navigation.registerComponent('Categories', () => Categories);
    Navigation.registerComponent('Featured', () => Featured);
    Navigation.registerComponent('ListWallpapers', () => ListWallpapers);
    Navigation.registerComponent('DisplayImage', () => DisplayImage);
    Navigation.registerComponent('Search', () => Search);
}