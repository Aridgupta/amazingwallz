import React, {Component} from 'react';
import {
    View, 
    Text, 
    Dimensions, 
    TouchableOpacity, 
    FlatList,
    TextInput
    } from 'react-native';
import Image from 'react-native-image-progress';
import * as Progress from 'react-native-progress';
import {List, ListItem, Card, Button, Icon, SearchBar} from 'react-native-elements';
import Unsplash, {toJson} from 'unsplash-js/native';
import {AdMobBanner} from 'react-native-admob';

const WIDTH = Dimensions.get('window').width;

export default class Search extends Component{
    static navigatorStyle = {
        navBarHidden: true,
        statusBarColor: '#212121',
        statusBarTextColorScheme: 'light',
        navBarBackgroundColor: '#000',
        navBarTextColor: '#fff',
        navBarButtonColor: '#fff'
    };

    constructor(props){
        super(props);
        this.state= {
            text: '',
            walls: [],
            page: 1,
            perPage: 30,
            loading: true
        }
    }

    fetchWalls = () => {
       unsplash.search.photos(this.state.text, this.state.page, this.state.perPage)
        .then(toJson)
        .then(json => {
            var wallImages = [];
            for(var x in json){
                wallImages.push(json[x])
            }
            wallImages = wallImages[2];
            this.setState({
                walls: [...this.state.walls, ...wallImages],
                loading: false
            });
            console.log(wallImages);
        })
        .catch(err=>{
            console.log(err)
        });
    };

    handleSearch = () => {
        this.setState({walls: [], page: 1})
        this.fetchWalls();
    }

    handleLoadMore = () => {
        this.setState({
            page: this.state.page + 1,
        }, () =>{
            this.fetchWalls();
        })
    };

    /* renderIndicator = () => {
        return(
            <View style={{flex:1, justifyContent: 'center', alignItems: 'center'}}>
                <Progress.CircleSnail 
                    color='#228b22'
                    size={80}
                />
            </View>
        );
    }; */

    renderFooter = () => {
        if(this.state.loading){
            return null;
        } else {
            return(
                <View style={{flex:1, justifyContent: 'center', alignItems: 'center'}}>
                    <Progress.CircleSnail 
                        color='#228b22' 
                        size={80}
                    />
                </View>
            );
        }
    };

    render(){
        const {container, header, imageStyles, adStyle} = styles;
        return(
            <View style={container}>
                <View style={header}>
                    <Icon name='keyboard-backspace' size={30} color='#fff' underlayColor='#a9a9a9' onPress={
                        ()=> {
                            console.log("Button Pressed!");
                            this.props.navigator.popToRoot({
                            animationType: 'slide-down'
                            })
                        }
                    }/>
                    <TextInput 
                        onChangeText={(text) => this.setState({text})}
                        onSubmitEditing={this.handleSearch}
                        placeholder='Search Photos from Unsplash'
                        placeholderTextColor='#fff'
                        returnKeyType='search'
                        style={{width: WIDTH/100*84, fontSize: 16, height: 40, color: '#fff'}}
                        underlineColorAndroid='#212121'
                        selectTextOnFocus={true}
                        autoFocus={true}
                    />
                </View>
                <List containerStyle={{marginTop: 0}}>
                    <FlatList
                        data={this.state.walls}
                        renderItem={({ item }) => (
                            <TouchableOpacity
                            onPress={() => this.props.navigator.push({
                                screen: 'DisplayImage',
                                passProps:{
                                    itemID: item.id,
                                    linkRaw: item.urls.raw,
                                    linkRegular: item.urls.regular,
                                    userName: item.user.name,
                                    userLink: item.user.links.html,
                                    userProfileImage: item.user.profile_image.small,
                                    userLocation: item.user.location,
                                    itemLikes: item.likes,
                                    date: item.created_at,
                                },
                                animated: true,
                                animationType: 'slide-horizontal'
                            }
                            )}>
                                <View style={container}>
                                    <Image
                                        source={{uri: item.urls.small}}
                                        style={imageStyles}
                                    />
                                </View>
                            </TouchableOpacity>
                        )}
                        numColumns={2}
                        keyExtractor={(item, index) => index}
                        onEndReached={this.handleLoadMore}
                        onEndThreshold={0.5}
                        ListFooterComponent={this.renderFooter}
                    />
                </List>
                <View style={adStyle}>
                    <AdMobBanner
                        bannerSize="smartBanner"
                        adUnitID="ca-app-pub-4831095411086994/1897006775"
                        didFailToReceiveAdWithError={()=> console.log('Banner Error')} />
                </View>
            </View>
        );
    }
}

const styles = {
    container: {
        flex: 1,
        backgroundColor: '#fff'
    },
    header:{
        flexDirection: 'row',
        height: 60,
        backgroundColor: '#212121',
        justifyContent: 'space-between',
        padding: 10,
        elevation: 5
    },
    imageStyles:{
        height: WIDTH / 2,
        width: WIDTH / 2,
    },
    adStyle: {
        position: 'absolute',
        bottom: 0
    }
}