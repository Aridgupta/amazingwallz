import {NativeModules} from 'react-native';

export const setImageAsWallpaper = (uri) => {
    NativeModules.SetWall.setImageAsWallpaper(uri)
}